package com.adeline64.naturecollection

class PlantModel (
    val id: String = "plant0",
    val name: String = "tulipe",
    val description: String = "Petite desccription",
    val imageUrl : String = "https://st4.depositphotos.com/thumbs/13324256/image/19660/196602968/api_thumb_450.jpg?forcejpeg=true",
    val grow: String = "Faible",
    val water: String = "Moyenne",
    var liked : Boolean = false
)