package com.adeline64.naturecollection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.adeline64.naturecollection.fragments.HomeFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // charger notre repository
        val repo = PlantRepository()

        // mettre à jour la liste de plantes
        repo.updateData {
        }
    }

    override fun onPostResume() {
        super.onPostResume()

        // vérifier si l'activité est en cours de destruction
        if (!isFinishing) {

            // injecter le fragment dans notre boite (fragment_container)
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, HomeFragment(this))
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
}
