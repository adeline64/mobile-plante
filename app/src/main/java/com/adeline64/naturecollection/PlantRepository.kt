package com.adeline64.naturecollection

import com.adeline64.naturecollection.PlantRepository.Singleton.databaseRef
import com.adeline64.naturecollection.PlantRepository.Singleton.plantList
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class PlantRepository {

    object Singleton {
        // se connecter à la référence "plants"
        val databaseRef = FirebaseDatabase.getInstance().getReference("plants")

        // créer une liste qui va contenir nos plantes
        val plantList = arrayListOf<PlantModel>()
    }

    fun updateData(callback: () -> Unit) {

        // absorber les données depuis la databaseRef pour ensuite les données à la liste de plantes
        databaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                // retirer les anciennes plant
                plantList.clear()

                // Parcourir les données de la base de données et créer des objets plante à partir de chaque entrée
                for (ds in snapshot.children) {

                    // construire un objet plante
                    val plant = ds.getValue(PlantModel::class.java)

                    // vérifier que la plante n'est pas null
                    if (plant != null) {

                        // ajouter la plante à notre liste
                        plantList.add(plant)
                    }

                }

                // actionner le callback
                callback()

            }

            override fun onCancelled(error: DatabaseError) {
                // Gestion d'erreur en cas d'annulation de la requête
            }

        })
    }

    // mettre à jour un objet plante en bdd
    fun updatePlant(plant: PlantModel) = databaseRef.child(plant.id).setValue(plant)

    // supprimer une plante de la base
    //fun deletePlant(plant: PlantModel) = databaseRef.child(plant.id).removeValue()
}